import Config from '../Config';

class SearchApiClient {
    private static readonly API_URL = Config.API_URL;

    public async searchMissions(term: string): Promise<any[]> {
        const url = SearchApiClient.API_URL + 'search/' + (term || '*');

        return fetch(url)
            .then(response => response.json())
            .then(json => json.map((entry: any) => entry._source))
        ;
    }
}

export default new SearchApiClient();
