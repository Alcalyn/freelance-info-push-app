import React from 'react';
import { Provider as PaperProvider, Button, Title, Searchbar, Card, Paragraph, DefaultTheme } from 'react-native-paper';
import { View, AppRegistry } from 'react-native';
import { expo } from './app.json';
import searchApiClient from './src/services/searchApiClient';

export default function App() {
  const [searchQuery, setSearchQuery] = React.useState('');
  const [missions, setMissions] = React.useState<any[]>([]);

  const searchMissions = (term: string) => {
    console.log('search for term', term);

    searchApiClient
      .searchMissions(term)
      .then(foundMissions => {
        console.log(foundMissions);
        setMissions(foundMissions);
      })
    ;
  };

  const onChangeSearch = (query: string): void => {
    setSearchQuery(query);
    searchMissions(query);
  };

  return (
    <PaperProvider>
      <Title>Freelance info missions</Title>
      <Searchbar
        placeholder='Rechercher une mission'
        onChangeText={onChangeSearch}
        onIconPress={() => searchMissions(searchQuery)}
        value={searchQuery}
      />
      <View>
        {missions.map((mission, index) => (
          <Card key={index}>
            <Card.Content>
              <Title>{mission.jobTitle}</Title>
              <Paragraph>{mission.description.substr(0, 100)}...</Paragraph>
            </Card.Content>
          </Card>
        ))}
      </View>
    </PaperProvider>
  );
}

AppRegistry.registerComponent(expo.name, () => App);
